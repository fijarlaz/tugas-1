from django.shortcuts import render
from .models import Event, Participant
from .forms import ParticipantForm
from django.http import HttpResponseRedirect
from . import forms
# Create your views here.

response ={}
def event(request):
    event = Event.objects.all()
    participant = Participant.objects.all()
    response['all_event'] = event
    response['participant'] = participant
    html = 'event.html'
    return render(request, html, response)


def join_event(request):
    response['join_event'] = ParticipantForm
    html = 'join-event.html'
    return render(request, html, response)

def participate(request):
	form = forms.ParticipantForm(request.POST)
	# objek_member = Event.objects.values_list('username')
	if (form.is_valid()):
		response['nama'] = request.POST['nama']
		response['username'] = request.POST['username']
		response['email'] = request.POST['email']
		response['event'] = form.cleaned_data['event']
		member = Participant(nama = response['nama'], username = response['username'], email = response['email'], event = response['event'])
		member.save()
		return HttpResponseRedirect('/events/')
	else:
		form = forms.ParticipantForm()

	return render(request, 'event.html', response)
