from . import views
from django.urls import path, include

urlpatterns = [
    path('', views.event, name='event'),
    path('join-event/', views.join_event, name='join_event'),
    path('participate/', views.participate, name='participate'),
]
