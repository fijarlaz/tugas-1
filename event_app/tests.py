from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import event
# Create your tests here.
class EventTest(TestCase):
    def test_is_url_exist(self):
        response = Client().get('/events/')
        self.assertEqual(response.status_code, 200)
    
    def test_event_page_using_index_func(self):
        found = resolve('/events/')
        self.assertEqual(found.func, event)
    
    def test_template_event_is_used(self):
        response = Client().get('/events/')
        self.assertTemplateUsed(response, 'event.html')

    