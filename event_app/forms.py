from django import forms
from .models import Event

class ParticipantForm(forms.Form):

    attrs= {
        'class' : 'form-control',
    }

    name_attrs = {
        'class' : 'form-control'
    }

    email_attrs = {
        'class' : 'form-control'
    }

    nama = forms.CharField(max_length=100, required=True, widget= forms.TextInput(attrs=name_attrs))
    username = forms.CharField(required = True, max_length = 100, widget = forms.TextInput(attrs=attrs))
    email = forms.EmailField(required=True, widget= forms.TextInput(attrs=email_attrs))
    event = forms.ModelChoiceField(queryset=Event.objects.all(), label='Event', widget=forms.Select)
