"""ppw_tugas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include
from django.contrib.auth import views
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
	path('', include(('form_app.urls','form_app'), namespace = 'member')),
    path('', include(('home_app.urls','home_app'), namespace= 'home')),
#path('accounts/', include('form_app.urls'), namespace='accou'),
    path('events/', include(('event_app.urls','event_app'), namespace='event')),
    path('news/', include(('news_app.urls', 'news_app'), namespace='news')),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), {'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
    url(r'^auth/', include('social_django.urls', namespace='social')),
]
