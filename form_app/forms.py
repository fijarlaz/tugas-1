from django import forms
from .models import Member
from django.forms import ModelForm
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User

class Registration_Form(forms.ModelForm):
	error_messages = {
        'required': 'Tolong isi input ini',
    }
	nama_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Name'
    }
	username_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Username'
    }
	email_attrs = {
        'type': 'email',
        'class': 'form-control',
        'placeholder':'Email'
    }
	tanggal_attrs = {
        'type': 'date',
        'class': 'form-control',
        'placeholder':'Tanggal Lahir'
    }
	pass_attrs = {
        'type': 'password',
        'class': 'form-control',
        'placeholder':'Password',
		'required pattern' : '[a-zA-Z0-9]+',
		'title' : 'Harus Alpha Numeric'
    }
	alamat_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder':'Alamat'
    }
	nama = forms.CharField(widget=forms.TextInput(attrs=nama_attrs))
	username = forms.CharField(max_length = 20,widget=forms.TextInput(attrs=username_attrs))
	email = forms.EmailField(widget=forms.TextInput(attrs=email_attrs))
	tanggal_lahir = forms.DateField(widget=forms.TextInput(attrs=tanggal_attrs))
	password = forms.CharField(min_length = 8 ,widget=forms.TextInput(attrs=pass_attrs))
	alamat = forms.CharField(max_length = 300,widget=forms.Textarea(attrs=alamat_attrs))
	class Meta :
		model = Member
		fields = ('nama','username','email', 'tanggal_lahir','password','alamat',)
		
	def clean_username(self):
		user = self.cleaned_data.get('username')
		username_qs = User.objects.filter(username=user)
		if username_qs.exists():
			raise ValidationError("Username already exists")
		return user

	def clean_email(self):
		email = self.cleaned_data.get('email')

        # Check to see if any users already exist with this email as a username.
		try:
			match = User.objects.get(email=email)
		except User.DoesNotExist:
            # Unable to find a user, this is fine
			return email

        # A user was found with this as a username, raise an error.
		raise forms.ValidationError('This email address is already in use.')
		
