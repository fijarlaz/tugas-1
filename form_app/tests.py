from django.test import TestCase,Client
from django.urls import resolve
from .views import index
from .models import Member
from .forms import Registration_Form

class FormAppUnitTest(TestCase):
	def test_registration_url_exist(self):
		response = Client().get('/registration/')
		self.assertEqual(response.status_code,200)

	def test_registration_using_template(self):
		response = Client().get('/registration/')
		self.assertTemplateUsed(response, 'registration.html')

	def test_registration_using_index_func(self):
		found = resolve('/registration/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_member(self):
		#Creating a new member
		new_member = Member.objects.create(nama = 'I Made Adisurya',username = 'adisryn10',email = 'i.made712@ui.ac.id',
										   tanggal_lahir = '1999-03-10',alamat = 'taman depok permai')
		#Retrieving all available member
		counting_all_available_member = Member.objects.all().count()
		self.assertEqual(counting_all_available_member,1)

	def test_form_todo_input_has_placeholder_and_css_classes(self):
		form = Registration_Form()
		self.assertIn('class="form-control', form.as_p())

	def test_form_validation_for_blank_items(self):
		form = Registration_Form(data={'nama' : '','username' : '','email' : '', 'tanggal_lahir' : '','alamat' : ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['nama'],
			["This field is required."]
		)











# Create your tests here.
