from django.shortcuts import render, redirect
from .forms import Registration_Form
from . import forms
from django.http import HttpResponseRedirect
from .models import Member

response = {}
def index(request):
	response['form'] = forms.Registration_Form
	response['member'] = Member.objects.all()
	html = 'registration.html'
	return render(request, html , response)

def add_member(request):
	form = forms.Registration_Form(request.POST)
	objek_member = Member.objects.values_list('username')
	if (form.is_valid()):
		response['nama'] = request.POST['nama']
		response['username'] = request.POST['username']
		response['email'] = request.POST['email']
		response['tanggal_lahir'] = request.POST['tanggal_lahir']
		response['password'] = request.POST['password']
		response['alamat'] = request.POST['alamat']
		member = Member(nama = response['nama'], username = response['username'], email = response['email'],
						tanggal_lahir = response['tanggal_lahir'], password = response['password'], alamat = response['alamat'])
		member.save()
		return HttpResponseRedirect('/halaman_utama')
	else:
		form = forms.Registration_Form()

	return render(request, 'registration.html', response)
