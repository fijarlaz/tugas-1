from django.db import models
from datetime import datetime, date
from django.core.validators import MinLengthValidator

# Create your models here.
class Member(models.Model):
	nama = models.CharField(max_length = 50)
	username = models.CharField(max_length = 20, unique = True, blank = True)
	email = models.EmailField(unique = True, error_messages={'unique': 'An account with this email exist.'})
	tanggal_lahir = models.DateField()
	password = models.CharField(validators=[MinLengthValidator(4)], max_length = 20)
	alamat = models.TextField(max_length = 300)
