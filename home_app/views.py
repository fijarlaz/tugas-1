from django.shortcuts import render
from event_app.models import Event
from news_app.models import News

# Create your views here.


def home_app(request):
    return render(request, 'index.html')

def halaman_utama(request):
    response = {'object_news' : News.objects.all()[:2], 'object_event' : Event.objects.all()[:3]}
    return render(request, 'utama.html',response)
