# Tugas 1 Kelompok 6 kelas PPW-C

We are from group 6 of class C of PPW 2018
Members of this group:
Name - NPM:
1. M Fijar Lazuardy - 1706039471
2. Gde Indra Raditya Nugraha - 1706984606
3. I Made Adisurya Nugraha - 1706984625
4. Muhammad Feril Bagus Perkasa - 1706075054

## App status
[![Pipeline](https://gitlab.com/kel6-ppwC/tugas-1/badges/master/pipeline.svg)](https://gitlab.com/kel6-ppwC/tugas-1/commits/master)
[![Coverage](https://gitlab.com/kel6-ppwC/tugas-1/badges/master/coverage.svg)](https://gitlab.com/kel6-ppwC/tugas-1/commits/master)


## Herokuapp link

https://tugas1-kelompok6-ppw-c.herokuapp.com/