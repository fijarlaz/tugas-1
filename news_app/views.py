from django.shortcuts import render
from .models import News
# Create your views here.
response = {}
def news(request):
    response['news'] = News.objects.all()
    return render(request, 'news.html',response)

# Create your views here.
